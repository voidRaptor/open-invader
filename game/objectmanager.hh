#ifndef OBJECT_MANAGER_HH
#define OBJECT_MANAGER_HH

#include "shipfactory.hh"

const float ENEMY_BOSS_COUNT = 5;
const float ENEMY_BOMBER_COUNT = 5;
const float ENEMY_FIGHTER_COUNT = 5;

void initObjects(
    std::vector<Sprite*>& objects,
    ShipFactory& factory)
{
    auto player = factory.create(ShipType::Player);
    player->model.pos = {-1.0f, 0.0f, -2.0f};
    player->state = ObjectState::Alive;
    objects.push_back(player);

    for (unsigned int i=0; i<ENEMY_FIGHTER_COUNT; ++i)
    {
        auto sprite = factory.create(ShipType::Fighter);
        sprite->model.pos = {-1.0f, -1.0f * i, -2.0f};
        objects.push_back(sprite);
    }

    for (unsigned int i=0; i<ENEMY_BOMBER_COUNT; ++i)
    {
        auto sprite = factory.create(ShipType::Bomber);
        sprite->model.pos = {-3.0f, -1.0f * i, -2.0f};
        objects.push_back(sprite);
    }

    for (unsigned int i=0; i<ENEMY_BOSS_COUNT; ++i)
    {
        auto sprite = factory.create(ShipType::Boss);
        sprite->model.pos = {-5.0f, -1.0f * i, -2.0f};
        objects.push_back(sprite);
    }

}

#endif
