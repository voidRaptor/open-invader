#ifndef GAME_HH
#define GAME_HH

#include "shipfactory.hh"

#include <engine/window.hh>

#include <engine/models/sprite.hh>
#include <engine/models/rect.hh>

#include <engine/render/shader.hh>
#include <engine/render/mesh.hh>
#include <engine/render/renderer.hh>

#include <engine/utility/time/stopwatch.hh>
#include <engine/utility/camera.hh>
#include <engine/utility/texturemanager.hh>
#include <engine/utility/keyboardinput.hh>


class Game
{
public:
    Game();
    ~Game();

    bool init();
    void run();

private:
    void handlePlayerInput(Sprite* player);
    void handleScreenBounds(Sprite* obj);

    MeshMap mMeshes;
    ShaderMap mShaders;
    std::vector<Sprite*> mObjects;

    Window mWindow;
    Camera mCamera;
    KeyboardInput mInput;
    TextureManager mTm;
    ShipFactory mShipFactory;
};

#endif
