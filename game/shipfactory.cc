#include "shipfactory.hh"

#include <engine/models/sprite.hh>
#include <engine/render/mesh.hh>
#include <engine/render/shader.hh>
#include <engine/utility/texturemanager.hh>

ShipFactory::ShipFactory(TextureManager& tm, ShaderMap& mShaders, MeshMap& mMeshes):
    mTm(tm),
    mShaders(mShaders),
    mMeshes(mMeshes)
{}


Sprite* ShipFactory::create(ShipType type)
{
    Sprite* sprite = new Sprite;
    sprite->mesh = mMeshes["texture"];
    sprite->shader = mShaders["texture"];

    switch (type)
    {
        case ShipType::Fighter:
            configFighter(sprite);
            break;

        case ShipType::Bomber:
            configBomber(sprite);
            break;

        case ShipType::Boss:
            configBoss(sprite);
            break;

        case ShipType::Player:
            configPlayer(sprite);
            break;
    }

    sprite->model.scale.x = sprite->model.scale.z * 128.0f / sprite->tex->height;
    sprite->model.scale.y = sprite->model.scale.z * 128.0f / sprite->tex->width;

    return sprite;
}


void ShipFactory::configFighter(Sprite* sprite)
{
    sprite->tex = mTm.getTexture("enemy_fighter");
    sprite->model.scale.z = 1.0f;
}

void ShipFactory::configBomber(Sprite* sprite)
{
    sprite->tex = mTm.getTexture("enemy_bomber");
    sprite->model.scale.z = 3.0f;
}

void ShipFactory::configBoss(Sprite* sprite)
{
    sprite->tex = mTm.getTexture("enemy_boss");
    sprite->model.scale.z = 3.5f;
}

void ShipFactory::configPlayer(Sprite* sprite)
{
    sprite->tex = mTm.getTexture("player");
    sprite->model.scale.z = 1.0f;
}

