#ifndef DATASTRUCTURES_HH
#define DATASTRUCTURES_HH

#include <map>
#include <string>

class Shader;
class Mesh;

using ShaderMap = std::map<std::string, Shader*>;
using MeshMap = std::map<std::string, Mesh*>;


#endif

