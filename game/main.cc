#include "game.hh"

#include <iostream>

int main(int argc, char** argv)
{
    /*
       opengl coordinate system:

      y ^
        |
        |___> x
       /
      /
     v z

    */
    Game gm;
    if (gm.init())
        gm.run();
    else
        std::cerr << "shutting down...\n";

    return 0;
}

