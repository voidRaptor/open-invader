#ifndef SHIPFACTORY_HH
#define SHIPFACTORY_HH

#include <map>
#include <string>

#include "datastructures.hh"

class TextureManager;
class Sprite;
class Shader;
class Mesh;

enum class ShipType
{
    Fighter,
    Bomber,
    Boss,
    Player
};

class ShipFactory
{
public:
    ShipFactory(TextureManager& tm, ShaderMap& shaders, MeshMap& meshes);
    Sprite* create(ShipType type);

private:
    void configFighter(Sprite* sprite);
    void configBomber(Sprite* sprite);
    void configBoss(Sprite* sprite);
    void configPlayer(Sprite* sprite);

    TextureManager& mTm;
    ShaderMap& mShaders;
    MeshMap& mMeshes;
};






#endif
