#ifndef SHADER_LOADER_HH
#define SHADER_LOADER_HH

#include "datastructures.hh"
#include <engine/utility/utility.hh>
#include <vector>
#include <string>
#include <filesystem>
#include <algorithm>


bool loadShaders(const std::string dir, ShaderMap &shaders)
{
    std::vector<std::string> paths;

    for (const auto &entry : std::filesystem::directory_iterator(dir))
        paths.push_back(entry.path());

    if (paths.size() % 2 != 0)
    {
        std::cout << "missing shader\n";
        return false;
    }

    std::sort(paths.begin(), paths.end());

    std::vector<std::string> splits;

    for (unsigned int i=0; i<paths.size(); i+=2)
    {
        Utility::split(splits, paths[i], "/");

        std::string name = splits[splits.size()-1];
        name = name.substr(0, name.size()-3);

        auto result = shaders.find(name);

        if (result == shaders.end())
            shaders.emplace(name, new Shader(paths[i], paths[i+1]));
        else
            std::cerr << "name exists\n";
    }

    return true;
}


std::string getShaderDir()
{
    auto info = Utility::getGpuInfo();
    std::string path;
    std::cout << "max shader version: " << info.shaderVersion << " -> ";

    if (info.shaderVersion > 1.3f)
    {
        std::cout << "using modern shaders\n";
        path = "res/shaders/modern";

    }
    else
    {
        std::cout << "using legacy shaders\n";
        path = "res/shaders/legacy";
    }

    return path;
}


#endif
