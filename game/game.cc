#include "game.hh"
#include "objectmanager.hh"
#include "shaderloader.hh"

#include <engine/utility/utility.hh>

#include <3rdParty/glm/glm/vec3.hpp>
#include <SDL2/SDL.h>

#include <map>
#include <iostream>
#include <memory>

const float ASPECT = 10.0f / 12.0f;
const float WIDTH = 800;
const float HEIGHT = 600;//WIDTH / ASPECT;


Game::Game():
    mWindow(WIDTH, HEIGHT),
    mCamera(WIDTH, HEIGHT),
    mShipFactory(mTm, mShaders, mMeshes)
{}


Game::~Game()
{
    for (auto& it : mMeshes) delete it.second;
    for (auto& it : mShaders) delete it.second;
    for (auto& it : mObjects) delete it;
}


void Game::run()
{
    Rect rect;
    rect.model.pos = {1.0f, 0.0f, -2.0f};
    rect.model.color = {0.0f, 1.0f, 0.0f};
    rect.mesh = mMeshes["rect"];
    rect.shader = mShaders["rect"];

    Stopwatch frameTimer;
    frameTimer.start();

    Stopwatch fpsTimer;
    fpsTimer.start(1.0f);

    Utility::printGpuInfo(Utility::getGpuInfo());
    SDL_Event e;

    while (!mInput.shouldQuit(&e))
    {
        double deltatime = frameTimer.getDeltaTime();

        mWindow.clear();
        mCamera.update(deltatime);
        rect.draw(&mCamera);

        handlePlayerInput(mObjects[0]);
        handleScreenBounds(mObjects[0]);

        for (auto& o : mObjects)
            o->drawAndBind(&mCamera);

        mWindow.update();

        if ( fpsTimer.isTimeout() )
        {
            mWindow.updateFPScounter(deltatime);
            fpsTimer.reset();
        }
    }

}


bool Game::init()
{
    bool error = false;
    error |= mTm.loadTexture("res/textures/wall_line.png", "wall") == nullptr;
    error |= mTm.loadTexture("res/textures/solaris_128.png", "enemy_fighter") == nullptr;
    error |= mTm.loadTexture("res/textures/cyclops_128.png", "enemy_bomber") == nullptr;
    error |= mTm.loadTexture("res/textures/phantom_128.png", "enemy_boss") == nullptr;
    error |= mTm.loadTexture("res/textures/crimson_128.png", "player") == nullptr;

    mMeshes.emplace("rect", new Mesh);
    mMeshes.emplace("texture", new Mesh(MeshType::SPRITE));

    std::string dir = getShaderDir();
    error |= !loadShaders(dir, mShaders);

    initObjects(mObjects, mShipFactory);

    return !error;
}


void Game::handlePlayerInput(Sprite* player)
{
    const Uint8* keyStates = SDL_GetKeyboardState(NULL);

    const float speed = 0.0005f;

    int yDir = 0;
    int xDir = 0;

    // up
    if (keyStates[SDL_SCANCODE_W])      yDir = 1;
    else if (keyStates[SDL_SCANCODE_S]) yDir = -1;

    // left
    if (keyStates[SDL_SCANCODE_A])      xDir = -1;
    else if (keyStates[SDL_SCANCODE_D]) xDir = 1;

    // no double speed if diagonal movement
    if (xDir != 0 && yDir != 0)
    {
        player->model.pos.x += xDir * speed * 0.5f;
        player->model.pos.y += yDir * speed * 0.5f;

    }
    else
    {
        player->model.pos.x += xDir * speed;
        player->model.pos.y += yDir * speed;
    }
}


void Game::handleScreenBounds(Sprite* obj)
{
    glm::vec4 posMat = mCamera.getProjection() * mCamera.getView() * glm::vec4(obj->model.pos, 1.0f);

    assert(posMat.w != 0.0f);
    auto normDevCoord = glm::vec2(posMat.x / posMat.w, posMat.y / posMat.w);
    auto pos = glm::vec3(
        (normDevCoord.x + 1.0f) / 2.0f * WIDTH,
        (1.0f - normDevCoord.y) / 2.0f * HEIGHT,
        1.0f
    );
    // TODO: take z or zoom level into account
    float w = obj->tex->width  * obj->model.scale.x;
    float h = obj->tex->height * obj->model.scale.y;

    /*
    std::cout << "transformed: " << pos.x << ", " << pos.y
              << " w: " << w
              << " h: " << h
              << " scale: " << obj->model.scale.x << "x" << obj->model.scale.y
              << "\n";

    if (pos.x - w / 2.0f < 0.0f)
    {
        //obj->model.pos.x = w / 2.0f;
        std::cout << "OOB: left\n";
    }
    else if (pos.x + w / 2.0f > WIDTH)
    {
        //obj->model.pos.x = WIDTH - w / 2.0f;
        std::cout << "OOB: right\n";
    }

    if (pos.y - h / 2.0f < 0.0f)
    {
        //obj->model.pos.y = h / 2.0f;
        std::cout << "OOB: top\n";
    }
    else if (pos.y + h / 2.0f > HEIGHT)
    {
        //obj->model.pos.y = HEIGHT - h / 2.0f;
        std::cout << "OOB: bottom\n";
    }
    */
}

