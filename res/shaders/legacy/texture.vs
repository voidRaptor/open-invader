#version 120

attribute vec3 aPos;
attribute vec2 aTexCoord;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

varying vec2 texCoord;


void main() {
    mat4 mvp = projection * view * model;

    gl_Position = mvp * vec4(aPos, 1.0f);
    texCoord = aTexCoord;
}

