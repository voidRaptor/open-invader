#version 120

varying vec4 FragColor;

uniform sampler2D tex;

varying vec2 texCoord;


void main() {
    vec4 texColor = texture2D(tex, texCoord);

    if (texColor.a < 0.1) {
        discard;
    }

    gl_FragColor = texColor;
}

