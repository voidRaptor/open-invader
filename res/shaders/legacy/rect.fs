#version 120

varying vec4 FragColor;

uniform sampler2D image;
uniform vec3 color;


void main() {
    gl_FragColor = vec4(color, 1.0);
}
