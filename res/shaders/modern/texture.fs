#version 450 core

out vec4 FragColor;

uniform sampler2D tex;

in vec2 texCoord;


void main() {
    vec4 texColor = texture2D(tex, texCoord);

    if (texColor.a < 0.1) {
        discard;
    }

    FragColor = texColor;
}

