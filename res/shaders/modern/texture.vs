#version 450 core

// attributes
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

out vec2 texCoord;


void main() {
    mat4 mvp = projection * view * model;

    gl_Position = mvp * vec4(aPos, 1.0f);
    texCoord = aTexCoord;
}

