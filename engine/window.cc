#include "window.hh"
#include <iostream>
#include <string>
#include <unordered_map>

const bool Window::ENABLE_VSYNC = true;
const glm::vec4 Window::CLEAR_COLOR = {1.0f, 0.3f, 0.3f, 1.0f};
const std::string Window::TITLE = "Open Invader";


const std::unordered_map<GLenum, std::string> SEVERITY_MAP =
{
    {GL_DEBUG_SEVERITY_HIGH,	"CRITICAL"},
    {GL_DEBUG_SEVERITY_MEDIUM, "HIGH WARNING"},
    {GL_DEBUG_SEVERITY_LOW,	"LOW WARNING"},
    {GL_DEBUG_SEVERITY_NOTIFICATION, "INFO"},
};

const std::unordered_map<GLenum, std::string> TYPE_MAP =
{
    {GL_DEBUG_TYPE_ERROR, "ERROR"},
    {GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, "DEPRECATED"},
    {GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR, "UNDEFINED_BEHAVIOUR"},
    {GL_DEBUG_TYPE_PORTABILITY, "PORTABILITY"},
    {GL_DEBUG_TYPE_PERFORMANCE, "PERFORMANCE"},
    {GL_DEBUG_TYPE_MARKER, "MARKER"},
    {GL_DEBUG_TYPE_PUSH_GROUP, "PUSH_GROUP"},
    {GL_DEBUG_TYPE_POP_GROUP, "POP_GROUP"},
    {GL_DEBUG_TYPE_OTHER, "OTHER"},
};

const std::unordered_map<GLenum, std::string> SOURCE_MAP =
{
    {GL_DEBUG_SOURCE_API, "API"},
    {GL_DEBUG_SOURCE_WINDOW_SYSTEM, "WINDOW_SYS"},
    {GL_DEBUG_SOURCE_SHADER_COMPILER, "SHADER_COMPILER"},
    {GL_DEBUG_SOURCE_THIRD_PARTY, "3RD_PARTY"},
    {GL_DEBUG_SOURCE_APPLICATION, "APP"},
    {GL_DEBUG_SOURCE_OTHER, "OTHER"},
};

void GLAPIENTRY errorCallback(
    GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar* message,
    const void* userParam)
{
    std::cerr << SEVERITY_MAP.find(severity)->second
              << " " << TYPE_MAP.find(type)->second
              << " " << SOURCE_MAP.find(source)->second
              << " -- " << message
              << "\n";
}


Window::Window(unsigned int width, unsigned int height):
    mInited(false),
    mWindow(nullptr),
    mContext(nullptr),
    mWidth(width),
    mHeight(height)
{
    mInited = init();
}


Window::~Window()
{
    SDL_GL_DeleteContext(mContext);
    SDL_DestroyWindow(mWindow);
    SDL_Quit();
}


SDL_Window* Window::getWindow()
{
    return mWindow;
}


void Window::clear()
{
    glClearColor(CLEAR_COLOR.x, CLEAR_COLOR.y, CLEAR_COLOR.z, CLEAR_COLOR.w);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


void Window::update()
{
    SDL_GL_SwapWindow(mWindow);
}


void Window::updateFPScounter(double deltatime)
{
    if (deltatime == 0.0f)
        return;

    std::string fps = std::to_string( 1.0f / deltatime );
    std::string title = TITLE + " | " + fps;

    SDL_SetWindowTitle(mWindow, title.c_str());
}


bool Window::isInited()
{
    return mInited;
}


bool Window::init()
{
    if ( SDL_Init(SDL_INIT_EVERYTHING) < 0 )
    {
        std::cerr << "SDL_Init: " << SDL_GetError() << "\n";
        return false;
    }

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    mWindow = SDL_CreateWindow(TITLE.c_str(),
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               mWidth,
                               mHeight,
                               SDL_WINDOW_OPENGL);

    if (mWindow == nullptr)
    {
        std::cerr << "SDL_Window: " << SDL_GetError() << "\n";
        return false;
    }

    mContext = SDL_GL_CreateContext(mWindow);

    if (glewInit() != GLEW_OK)
    {
        std::cerr << "Glew initialization failed.\n";
        return false;
    }

    SDL_GL_SetSwapInterval(0);

    // NOTE: depth test must not be enabled for 2D or last drawn will not
    // always in the front

    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(errorCallback, 0);

    return true;
}


