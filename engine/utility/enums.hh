#ifndef ENUMS_HH
#define ENUMS_HH

enum class ObjectState
{
    Default,
    Dead,
    Alive
};

#endif
