#ifndef CAMERA_HH
#define CAMERA_HH

#include "time/stopwatch.hh"

#include <3rdParty/glm/glm/vec3.hpp>
#include <3rdParty/glm/glm/ext.hpp>
#include <3rdParty/glm/glm/glm.hpp>
#include <3rdParty/glm/glm/gtc/quaternion.hpp>
#include <3rdParty/glm/glm/gtx/quaternion.hpp>

struct Acceleration
{
    float forward;      // towards or away from center of view
    float strafe;       // sideways
    float ascend;       // ascend (descend if negative)
    float pitch;        // camera up and down rotation
    float yaw;          // camera left and right rotation
};


struct CamCoord
{
    glm::vec3 pos;
    glm::vec3 dir;
    glm::vec3 up;
};


class Camera
{
public:
    Camera(int windowW, int windowH);
    ~Camera();

    glm::mat4 getViewProjection();
    glm::mat4 getView();
    glm::mat4 getProjection();

    glm::vec3 getPos();
    glm::vec3 getDir() const;
    glm::vec3 getUp() const;

    void update(double deltatime);

    glm::vec3 getPosition();


    const Acceleration& getAccel() const;

private:
    glm::vec3 mPos;
    Stopwatch mToggleTimer;

    glm::mat4 mPerspective;
    CamCoord mCoord;

    // Euler angles
    float mPitch;
    float mYaw;


    glm::quat mRotation;

    Acceleration mAccel;

};



#endif
