#include "keyboardinput.hh"

const float KeyboardInput::ENTER_DELAY = 0.5f;
const float KeyboardInput::MOUSE_BUTTON_DELAY = 0.3f;


KeyboardInput::KeyboardInput():
    mFirstEnterPress(true)
{
    mMouseLeftTimer.start(MOUSE_BUTTON_DELAY);
    mMouseMiddleTimer.start(MOUSE_BUTTON_DELAY);
    mMouseRightTimer.start(MOUSE_BUTTON_DELAY);
}


bool KeyboardInput::exitPressed()
{
    SDL_PumpEvents();

    // NOTE: checking q key with polls might cause crash
    // https://gamedev.stackexchange.com/questions/69615/why-does-my-sdl-code-crash-when-i-move-the-mouse-over-the-window
    const Uint8* keyStates = SDL_GetKeyboardState(NULL);
    return keyStates[SDL_SCANCODE_Q];
}


void KeyboardInput::handleWASD(glm::vec3* v)
{
    SDL_PumpEvents();
    const Uint8* keyStates = SDL_GetKeyboardState(NULL);

    if ( keyStates[SDL_SCANCODE_W] )      v->y = -1.0f;
    else if ( keyStates[SDL_SCANCODE_S] ) v->y =  1.0f;
    else                                  v->y =  0.0f;

    if ( keyStates[SDL_SCANCODE_A] )      v->x = -1.0f;
    else if ( keyStates[SDL_SCANCODE_D] ) v->x =  1.0f;
    else                                  v->x =  0.0f;
}


bool KeyboardInput::shiftPressed()
{
    SDL_PumpEvents();
    const Uint8* keyStates = SDL_GetKeyboardState(NULL);
    return keyStates[SDL_SCANCODE_LSHIFT];
}


bool KeyboardInput::enterPressed()
{
/*
    SDL_PumpEvents();
    const Uint8* keyStates = SDL_GetKeyboardState(NULL);

    if ( !keyStates[SDL_SCANCODE_RETURN] ) return false;


    // button debounce
    if (mFirstEnterPress) {
        mEnterTimer.start(ENTER_DELAY);
        mFirstEnterPress = false;

    } else if ( !mEnterTimer.isTimeout() ) {
        return false;

    } else mEnterTimer.reset();

    // std::cout << "accepted enter press\n";
*/
    return true;
}


bool KeyboardInput::shouldQuit(SDL_Event *e)
{
    while ( SDL_PollEvent(e) )
    {
        if (e->type == SDL_QUIT)
            return true;
    }

    SDL_PumpEvents();
    const Uint8* keyStates = SDL_GetKeyboardState(NULL);

    if ( keyStates[SDL_SCANCODE_Q] )
        return true;

    return false;
}

