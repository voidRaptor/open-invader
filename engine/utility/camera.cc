#include "camera.hh"
#include <SDL2/SDL.h>
#include <iostream>

static const float FOV = 70.0f;
static const float Z_NEAR = 0.1f;    // distance to closer zy clip plane
static const float Z_FAR = 1000.0f;  // distance to farther zy clip plane, also shortens distances on z axis
static const float START_DISTANCE = 5.0f;

void printKeyPress(const Uint8* keystates)
{
    for (unsigned int i=0; i<512; ++i)
    {
        if (keystates[i])
            std::cout << "pressed " << SDL_GetScancodeName((SDL_Scancode)i) << "\n";
    }
}


Camera::Camera(int windowW, int windowH):
    mPos{(float)windowW / 2.0f, (float)windowH / 2.0f, 0.0f},

    mPitch(0.0f),
    mYaw(90.0f),
    mRotation{0.0f, 0.0f, 0.0f, 0.0f},
    mAccel{0.0f, 0.0f, 0.0f, 0.0f, 0.0f}
{
    mCoord = {
        glm::vec3(0, 0, -3),
        glm::vec3(0, 0, -1),
        glm::vec3(0, 1, 0)
    };
    mCoord.pos += START_DISTANCE * mCoord.dir;

    // start dir
    mPitch = glm::degrees( asin(mCoord.dir.y));
    mYaw   = glm::degrees( atan2(mCoord.dir.x, mCoord.dir.z) / 2.0f );

    float aspect =  (float)windowW / (float)windowH;

    mPerspective = glm::perspective(FOV, aspect, Z_NEAR, Z_FAR);
}


Camera::~Camera()
{}


void Camera::update(double deltatime)
{
    const Uint8* keyStates = SDL_GetKeyboardState(NULL);

    // zoom in
    if (keyStates[SDL_SCANCODE_SLASH])      mAccel.forward = 1.0f;
    else if (keyStates[SDL_SCANCODE_MINUS]) mAccel.forward = -1.0f;
    else                                    mAccel.forward = 0.0f;

    /*
    // up
    if (keyStates[SDL_SCANCODE_W])      mAccel.ascend = 1.0f;
    else if (keyStates[SDL_SCANCODE_S]) mAccel.ascend = -1.0f;
    else                                mAccel.ascend = 0.0f;

    // left
    if (keyStates[SDL_SCANCODE_A])      mAccel.strafe = -1.0f;
    else if (keyStates[SDL_SCANCODE_D]) mAccel.strafe = 1.0f;
    else                                mAccel.strafe = 0.0f;
    */

    // super speed
    if (keyStates[SDL_SCANCODE_LCTRL])
    {
        mAccel.forward *= 3.0f;
        mAccel.strafe  *= 3.0f;
        mAccel.ascend  *= 3.0f;
    }

    // glm wants floats
    float camSpeed = deltatime;

    // update camera movement
    mCoord.pos +=  mAccel.forward * camSpeed * mCoord.dir;
    mCoord.pos +=  mAccel.strafe * glm::normalize( glm::cross(mCoord.dir, mCoord.up) ) * camSpeed;
    mCoord.pos.y += mAccel.ascend * camSpeed;
}


glm::vec3 Camera::getPosition()
{
    return mPos;
}


glm::mat4 Camera::getViewProjection()
{
    return mPerspective * getView();
}


glm::mat4 Camera::getView()
{
    /*
    return glm::lookAt(mCoord.pos, mCoord.pos + mCoord.dir, mCoord.up);
    */
    glm::mat4 view = glm::lookAt(mCoord.pos, mCoord.pos + mCoord.dir, mCoord.up);
    return glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, Z_NEAR, 0.001f * Z_FAR) * view;
}


glm::mat4 Camera::getProjection()
{
    return mPerspective;
}


glm::vec3 Camera::getPos()
{
    return mCoord.pos;
}


glm::vec3 Camera::getDir() const
{
    return mCoord.dir;
}


glm::vec3 Camera::getUp() const
{
    return mCoord.up;
}


const Acceleration& Camera::getAccel() const
{
    return mAccel;
}



