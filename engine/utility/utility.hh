#ifndef UTILITY_HH
#define UTILITY_HH

#include "../../3rdParty/glm/glm/vec3.hpp"
#include <vector>
#include <string>


/**
 * @brief collection of different utility functions
 */
namespace Utility {

/**
 * @brief convert world coordinates to screen coordinates
 *
 * @param world, position in world coordinates
 * @param offset, camera offset for the screen
 * @param scale
 *
 * @return position in screen coordinates
 */
glm::vec3 worldToScreen(
    const glm::vec3 &world,
    const glm::vec3 &offset,
    const glm::vec3 &scale={1.0f, 1.0f, 1.0f}
);


/**
 * @brief convert screen coordinates to world coordinates
 *
 * @param screen, position in screen coordinates
 * @param offset, camera offset for the screen
 * @param scale
 *
 * @return position in world coordinates
 */
glm::vec3 screenToWorld(
    const glm::vec3 &screen,
    const glm::vec3 &offset,
    const glm::vec3 &scale={1.0f, 1.0f, 1.0f}
);


/**
 * @brief map, map value from certain range to another
 *
 * @param min1, minimum of original range
 * @param max1, maximum of original range
 * @param min2, minum of second range
 * @param max2, maximum of second range
 * @return mapped value
 */
float map(
    const float value,
    const float min1,
    const float max1,
    const float min2,
    const float max2
);

/**
 * @brief random, return random number, [min, max]
 */
int random(const int &min, const int &max);


/**
 * @brief split, split string by separator into vector of strings
 *
 * @param vec, storage for split strings
 * @param s, string to split
 * @param separator, string which determines where to split
 * @param removeEmpty, whether empty string will be removed
 * @param removeSpaces, whether spaces will be removed
 */
void split(
    std::vector<std::string> &vec,
    const std::string &s,
    const std::string &separator,
    bool removeEmpty=true,
    bool removeSpaces=false
);


/**
* @brief calculate number of digits in a number
*
* @param x, number
*
* @return digits
*/
int digits(int x);


float vectorAngle2D(const glm::vec3 &a, const glm::vec3 &b);


struct GpuInfo
{
    std::string vendor;
    std::string model;
    float glVersionNumber;
    std::string glVersionFull;
    float shaderVersion;
};

GpuInfo getGpuInfo();

void printGpuInfo(const GpuInfo& info);

}

#endif
