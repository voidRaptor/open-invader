#ifndef TEXTUREMANAGER_HH
#define TEXTUREMANAGER_HH

#include "../render/texture.hh"
#include <string>
#include <unordered_map>
#include <SDL2/SDL.h>


/**
* @brief class for storing textures
*
*/
class TextureManager
{
public:
    TextureManager();
    ~TextureManager();

    /**
    * @brief load textures defined in configFile and stores them
    *
    * @return
    *   true if successful
    *   false if texture file is not found or multiple textures with same name
    */
    bool loadTextures(const std::string &configFile, bool verticalFlip);


    /**
    * @brief load single texture from image file and store it
    *
    * @param path, relative path to image (jpg/png)
    * @param name, identifier, with which texture can be retrieved from storage
    *
    * @return pointer to Texture
    *
    */
    Texture* loadTexture(const std::string &path, const std::string &name);

    /**
    * @brief load True Type font text as SDL texture
    *
    * @param path, relative path to font file
    * @param text, text to be loaded
    * @param fontsize (pixels)
    * @param color
    *
    * @return pointer to Texture
    *
    * @pre SDL_image is inited
    */

    /*
    SDL_Texture* loadTTF(SDL_Renderer* r,
                         const std::string &path,
                         const std::string &text,
                         int fontsize,
                         const SDL_Color &color);

    */

    /**
    * @brief search for texture
    *
    * @param name, name of texture defined in its config file
    *
    * @return
    *   if exists pointer to texture
    *   else nullptr
    */
    Texture* getTexture(const std::string &name);


    /**
    * @brief add new texture, if texture with name <name> exists, don't do anything
    *
    * @param name, name for texture to be used when searching
    *
    * @return
    *   true if successful
    *   false if name exists already
    */
    bool addTexture(const std::string &name, Texture* tex);


    /**
    * @brief delete texture from storage
    *
    * @param name, identifier
    *
    * @return
    *   true if successful
    *   false if texture with name doesn't exist
    */
    bool deleteTexture(const std::string &name);


private:
    std::unordered_map<std::string, Texture*> mTextures;


};


#endif
