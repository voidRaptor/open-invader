#ifndef KEYBOARDINPUT_HH
#define KEYBOARDINPUT_HH

#include <iostream>
#include <SDL2/SDL.h>
#include "time/stopwatch.hh"
#include "../../3rdParty/glm/glm/vec3.hpp"

/**
 * @brief singleton for handling keyboard input
 *
 */
class KeyboardInput
{
public:
    KeyboardInput();

    /**
     * @brief used to detect exit commands (q and window's X)
     * @param e, SDL eventhandler item
     */
    bool exitPressed();


    /**
    * @brief handle input for wasd, used for e.g. game object movement
    *
    * @param v, object for storing accelerations on each axis
    */
    void handleWASD(glm::vec3* v);


    /**
     * @brief detect if button was pressed
     * @return true if pressed
     */
    bool shiftPressed();
    bool enterPressed();


    bool shouldQuit(SDL_Event *e);

    // TODO: bool keyPressed(char key); -> map of buttons


private:
    bool mFirstEnterPress;

    Stopwatch mMouseLeftTimer;
    Stopwatch mMouseMiddleTimer;
    Stopwatch mMouseRightTimer;

    static const float ENTER_DELAY;
    static const float MOUSE_BUTTON_DELAY;
};


#endif
