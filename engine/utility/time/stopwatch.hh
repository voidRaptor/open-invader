#ifndef STOPWATCH_HH
#define STOPWATCH_HH

#include "hirestimer.hh"
#include <memory>


/**
 * @brief Stopwatch, simple wrapper for timers of different precisions
 *
 */
class Stopwatch 
{
public:
    Stopwatch(double timeout=-1.0f);
    ~Stopwatch();

    void reset();
    void start(double timeout=-1.0f);

    /**
     * @brief getTotalTime, return the total time from creating the object
     */
    double getTotalTime();

    /**
     * @brief getDeltaTIme, return elapsed time after previous call to this function
     */
    double getDeltaTime();

    /**
     * @brief isTimeout, return true if user set time limit has been reached
     */
    bool isTimeout();



private:
    std::unique_ptr<ITimer> mTimer;
    double mTimeout;
};


#endif
