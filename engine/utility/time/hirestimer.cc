#include "hirestimer.hh"
#include <iostream>

HiResTimer::HiResTimer()
{}


HiResTimer::~HiResTimer()
{}


double HiResTimer::getTotal()
{
    auto current = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = current - mTotalStart;
    return diff.count();
}


double HiResTimer::getDelta()
{
    auto current = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> delta = current - mDeltaStart;
    mDeltaStart = current;

    return delta.count();
}


void HiResTimer::start()
{
    mTotalStart = std::chrono::high_resolution_clock::now();
    mDeltaStart = std::chrono::high_resolution_clock::now();
}


