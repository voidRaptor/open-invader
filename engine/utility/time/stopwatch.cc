#include "stopwatch.hh"
#include "hirestimer.hh"


Stopwatch::Stopwatch(double timeout):
    mTimeout(timeout)
{
    mTimer = std::make_unique<HiResTimer>();
}


Stopwatch::~Stopwatch()
{}


void Stopwatch::reset()
{
    start(mTimeout);  // ensure same timeout is used on reset
}


void Stopwatch::start(double timeout)
{
    mTimeout = timeout;
    mTimer->start();
}


double Stopwatch::getTotalTime()
{
    return mTimer->getTotal();
}


double Stopwatch::getDeltaTime()
{
    return mTimer->getDelta();
}


bool Stopwatch::isTimeout()
{
    if (mTimeout > -1.0f)
        return (getTotalTime() > mTimeout);

    return false;
}

