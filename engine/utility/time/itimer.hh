#ifndef ITIMER_HH
#define ITIMER_HH

class ITimer
{
public:
    ITimer() = default;
    virtual ~ITimer() = default;

    /**
     * @brief getTotal, get total time elapsed after timer started
     */
    virtual double getTotal() = 0;

    /**
     * @brief getDelta, get time passed after previous call to this function
     */
    virtual double getDelta() = 0;

    /**
     * @brief start, start the timer
     */
    virtual void start() = 0;

};


#endif
