#include "utility.hh"

#include <3rdParty/glm/glm/gtc/constants.hpp>
#include <GL/glew.h>
#include <random>
#include <math.h>
#include <iostream>

namespace Utility {

glm::vec3 worldToScreen(
    const glm::vec3 &world,
    const glm::vec3 &offset,
    const glm::vec3 &scale)
{
    return (world - offset) * scale;
}


glm::vec3 screenToWorld(
    const glm::vec3 &screen,
    const glm::vec3 &offset,
    const glm::vec3 &scale)
{
    return screen / scale + offset;
}


float map(
    const float value,
    const float min1,
    const float max1,
    const float min2,
    const float max2)
{
    return min2 + ((max2 - min2) / (max1 - min1)) * (value - min1);
}

int random(const int &min, const int &max)
{
    return  min + rand() % (max-min+1);
}


void split(
    std::vector<std::string> &vec,
    const std::string &s,
    const std::string &separator,
    bool removeEmpty,
    bool removeSpaces)
{
    std::string::size_type begin = 0;
    std::string::size_type end = 0;
    std::string item = "";

    while (end != std::string::npos)
    {
        end = s.find(separator, begin);
        item = s.substr(begin, end-begin);

        if (item != "" || !removeEmpty)
        {
            if (removeSpaces)
            {
                for (auto it=item.begin(); it != item.end();)
                {
                    if (*it == ' ')
                        item.erase(it);
                    else
                        ++it;
                }
            }

            vec.push_back(item);
        }
        begin = end + separator.size();
    }

}


int digits(int x)
{
    x = abs(x);
    return (x < 10 ? 1 :
           (x < 100 ? 2 :
           (x < 1000 ? 3 :
           (x < 10000 ? 4 :
           (x < 100000 ? 5 :
           (x < 1000000 ? 6 :
           (x < 10000000 ? 7 :
           (x < 100000000 ? 8 :
           (x < 1000000000 ? 9 :
           10)))))))));
}


float vectorAngle2D(const glm::vec3 &a, const glm::vec3 &b)
{
    float mag_a = sqrt(a.x*a.x + a.y*a.y);
    float mag_b = sqrt(b.x*b.x + b.y*b.y);
    float dot = a.x*b.x + a.y*b.y;

    float angle = acos( dot / (mag_a * mag_b) ) * 180.0f/glm::pi<float>();

    if ( isnan(angle) )
        return 0.0f;

    if (a.x < b.x)
        return (360.0f - angle);

    return angle;
}

GpuInfo getGpuInfo()
{
    const GLubyte *renderer = glGetString( GL_RENDERER );
    const GLubyte *vendor = glGetString( GL_VENDOR );
    const GLubyte *version = glGetString( GL_VERSION );
    const GLubyte *glslVersion = glGetString( GL_SHADING_LANGUAGE_VERSION );

    GLint major, minor;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);

    std::vector<std::string> splits;
    Utility::split(splits, std::string((const char*)glslVersion), " ");

    GpuInfo info = {
        (const char*)vendor,
        (const char*)renderer,
        float(major) + float(minor) / 10.0f,
        (const char*)version,
        stof(splits[0])
    };

    return info;
}

void printGpuInfo(const GpuInfo& info)
{
    // TODO: log these automatically somewhere in engine
    std::cout << "/////////////////////////////"
              << "\nGPU Vendor        : " << info.vendor
              << "\nGPU Renderer      : " << info.model
              << "\nGL Version Number : " << info.glVersionNumber
              << "\nGL Version Full   : " << info.glVersionFull
              << "\nGLSL Version      : " << info.shaderVersion
              << "\n/////////////////////////////\n";
}


}

