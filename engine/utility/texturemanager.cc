#include "texturemanager.hh"
#include "utility.hh"
#include <SDL2/SDL_ttf.h>
#include "../3rdParty/stb_image/stb_image.h"
#include <GL/glew.h>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <cassert>


Texture* loadTextureStatic(const std::string &path, bool verticalFlip)
{
    // NOTE: stbi_load takes these as ints, not uints
    int width = 0;
    int height = 0;
    int channelCount = 0;

    unsigned int textureID = 0;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    stbi_set_flip_vertically_on_load(verticalFlip);
    unsigned char* img = stbi_load(path.c_str(), &width, &height, &channelCount, 0);

    if (img == NULL)
    {
        std::cerr << "Texture "
                  << path
                  << " loading failed.\n";

        stbi_image_free(img);
        return nullptr;

    }
    else
    {
        if (channelCount == 3)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
        else
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img);

        glGenerateMipmap(GL_TEXTURE_2D);
    }

    stbi_image_free(img);

    assert (channelCount == 3 || channelCount == 4);

    Texture* tex = new Texture();
    tex->id = textureID;
    tex->width = width;
    tex->height = height;

    return tex;
}


TextureManager::TextureManager()
{}


TextureManager::~TextureManager()
{
    for (auto it : mTextures)
    {
        it.second->free();
        delete it.second;
    }

}


bool TextureManager::loadTextures(const std::string &configFile, bool verticalFlip)
{
    std::fstream file(configFile);
    if (!file)
    {
        std::cerr << "failed to load config file '" << configFile << "'";
        return false;
    }

    std::string line;

    while ( getline(file, line) )
    {
        std::vector<std::string> tmp;
        Utility::split(tmp, line, " ", true);

        auto it = mTextures.find(tmp[0]);

        if ( it != mTextures.end() )
        {
            std::cerr << "texture already exists! continuing...\n";
        }
        else
        {
            Texture* tex = loadTextureStatic(tmp[1], false);
            mTextures.emplace_hint(it, tmp[0], tex);
        }
    }

    file.close();

    return true;
}


Texture* TextureManager::loadTexture(const std::string &path, const std::string &name)
{
    Texture* tex = loadTextureStatic(path, false);

    if (tex == nullptr)
    {
        std::cerr << "Failed to load texture \"" << name << "\"\n";
        return nullptr;
    }

    addTexture(name, tex);
    return tex;
}


/*
SDL_Texture* TextureManager::loadTTF(SDL_Renderer* r,
                                     const std::string &path,
                                     const std::string &text,
                                     int fontsize,
                                     const SDL_Color &color) {

    TTF_Font* font = TTF_OpenFont(path.c_str(), fontsize);

    if (font == nullptr) {
        std::cerr << "TTF_OpenFont: " << TTF_GetError() << "\n";
        return nullptr;
    }

    SDL_Surface* surface = TTF_RenderText_Solid(font, text.c_str(), color);
    TTF_CloseFont(font);

    if (surface == nullptr) {
        std::cerr << "TTF_RenderText_Solid: " << TTF_GetError() << "\n";
        return nullptr;
    }

    SDL_Texture* tex = SDL_CreateTextureFromSurface(r, surface);
    SDL_FreeSurface(surface);

    if (tex == nullptr) {
        std::cerr << "SDL_CreateTexture: " << SDL_GetError() << "\n";
        return nullptr;
    }

    return tex;

}
*/


Texture* TextureManager::getTexture(const std::string &name)
{
    auto it = mTextures.find(name);

    if ( it == mTextures.end() )
        return nullptr;

    return it->second;
}


bool TextureManager::addTexture(const std::string &name, Texture* tex)
{
    auto it = mTextures.find(name);

    if ( it != mTextures.end() )
    {
        std::cerr << "texture named '" << name << "' already exists\n";
        return false;
    }

    mTextures.emplace_hint(it, name, tex);
    return true;
}


bool TextureManager::deleteTexture(const std::string &name)
{
    auto it = mTextures.find(name);

    if ( it == mTextures.end() )
    {
        std::cerr << "texture named '" << name << "' doesn't exist\n";
        return false;
    }

    it->second->free();
    delete it->second;
    mTextures.erase(it);
    return true;
}


