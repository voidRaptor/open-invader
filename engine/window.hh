#ifndef WINDOW_HH
#define WINDOW_HH

#include "../3rdParty/glm/glm/vec4.hpp"

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <string>


/**
* @brief class for accessing the window created by SDL
*/
class Window
{
public:
    Window(unsigned int width, unsigned int height);
    ~Window();

    // TODO:
    void setSize(unsigned int w, unsigned int h);
    void setTitle(const std::string& title);

    SDL_Window* getWindow();

    /**
    * @brief clear screen with color CLEAR_COLOR
    */
    void clear();


    /**
    * @brief render drawn elements into framebuffer
    */
    void update();


    /**
    * @brief update window title's fps counter
    *
    * @param deltatime, time between frames
    */
    void updateFPScounter(double deltatime);

    bool isInited();



private:
    bool init();


    bool mInited;

    SDL_Window* mWindow;
    SDL_GLContext mContext;  // opaque ptr

    unsigned int mWidth;
    unsigned int mHeight;


    static const bool ENABLE_VSYNC;

    static const glm::vec4 CLEAR_COLOR;
    static const std::string TITLE;
};


#endif
