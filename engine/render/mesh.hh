#ifndef MESH_HH
#define MESH_HH

#include <3rdParty/glm/glm/vec3.hpp>
#include <3rdParty/glm/glm/vec4.hpp>
#include <3rdParty/glm/glm/matrix.hpp>
#include <array>

enum class MeshType
{
    DEFAULT,
    SPRITE,
    SPRITE_DYN,
};

class Mesh
{
public:
    Mesh(MeshType type=MeshType::DEFAULT);
    ~Mesh();

    void draw();

    // for sprite sheet
    void updateTexCoords(glm::vec4 &clip, float w, float h);

    void generateTextured(bool dynamic=false);

    std::array<glm::vec3, 6> vertices;
    std::array<glm::vec2, 6> texCoords;


private:
    unsigned int mVAO;  // vertex array object
    unsigned int mVBO;  // vertex buffer object
    unsigned int mTBO;  // texture buffer object
};


#endif
