#include "mesh.hh"
#include "../utility/camera.hh"
#include "../utility/utility.hh"
#include <GL/glew.h>
#include <vector>
#include <iostream>

Mesh::Mesh(MeshType type)
{
    vertices[0] = {-0.5f, -0.5f, 0.0f};
    vertices[1] = {-0.5f,  0.5f, 0.0f};
    vertices[2] = { 0.5f,  0.5f, 0.0f};
    vertices[3] = { 0.5f,  0.5f, 0.0f};
    vertices[4] = { 0.5f, -0.5f, 0.0f};
    vertices[5] = {-0.5f, -0.5f, 0.0f};

    glGenVertexArrays(1, &mVAO);
    glBindVertexArray(mVAO);

    glGenBuffers(1, &mVBO);

    // init position coords
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferData(
        GL_ARRAY_BUFFER,
        vertices.size() * sizeof(vertices[0]),
        &vertices[0],
        GL_STATIC_DRAW
    );

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    switch (type)
    {
        case MeshType::SPRITE:
            generateTextured(false);
            break;

        case MeshType::SPRITE_DYN:
            generateTextured(true);
            break;

        default:
            break;
    }
}


Mesh::~Mesh()
{
    glDeleteVertexArrays(1, &mVAO);
    glDeleteBuffers(1, &mVBO);
    glDeleteBuffers(1, &mTBO);
}


void Mesh::generateTextured(bool dynamic)
{
    texCoords[0] = {0.0f, 1.0f}; // top left
    texCoords[1] = {0.0f, 0.0f}; // bottom left
    texCoords[2] = {1.0f, 0.0f}; // bottom right
    texCoords[3] = {1.0f, 0.0f}; // bottom right
    texCoords[4] = {1.0f, 1.0f}; // top right
    texCoords[5] = {0.0f, 1.0f}; // top left

    glGenBuffers(1, &mTBO);

    // init texture coords
    glBindBuffer(GL_ARRAY_BUFFER, mTBO);

    if (dynamic)
    {
        glBufferData(
            GL_ARRAY_BUFFER,
            texCoords.size() * sizeof(texCoords[0]),
            &texCoords[0],
            GL_DYNAMIC_DRAW
        );
    }
    else
    {
        glBufferData(
            GL_ARRAY_BUFFER,
            texCoords.size() * sizeof(texCoords[0]),
            &texCoords[0],
            GL_STATIC_DRAW
        );
    }

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

}


// TODO: replace by passing texOffset to shader?
// https://stackoverflow.com/questions/27771902/opengl-changing-texture-coordinates-on-the-fly
void Mesh::updateTexCoords(glm::vec4 &clip, float w, float h)
{
    // top left
    float x0 = Utility::map(clip.x, 0.0f, w, 0.0f, 1.0f);
    float y0 = Utility::map(clip.y, 0.0f, h, 0.0f, 1.0f);

    // bottom left
    float x1 = Utility::map(clip.x, 0.0f, w, 0.0f, 1.0f);
    float y1 = Utility::map(clip.y + clip.w, 0.0f, h, 0.0f, 1.0f);

    // bottom right
    float x2 = Utility::map(clip.x + clip.z, 0.0f, w, 0.0f, 1.0f);
    float y2 = Utility::map(clip.y + clip.w, 0.0f, h, 0.0f, 1.0f);

    // top right
    float x3 = Utility::map(clip.x + clip.z, 0.0f, w, 0.0f, 1.0f);
    float y3 = Utility::map(clip.y, 0.0f, h, 0.0f, 1.0f);

    texCoords[0] = {x0, y0};
    texCoords[1] = {x1, y1};
    texCoords[2] = {x2, y2};
    texCoords[3] = {x2, y2};
    texCoords[4] = {x3, y3};
    texCoords[5] = {x0, y0};

    glBindBuffer(GL_ARRAY_BUFFER, mTBO);
    glBufferSubData(
        GL_ARRAY_BUFFER,
        0,
        texCoords.size() * sizeof(texCoords[0]),
        &texCoords[0]
    );


}


void Mesh::draw()
{
    glBindVertexArray(mVAO);
    glDrawArrays(GL_TRIANGLES, 0, vertices.size());
    glBindVertexArray(0);
}



