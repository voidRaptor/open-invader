#include "shader.hh"
#include <fstream>
#include <iostream>


Shader::Shader(const std::string &fragPath, const std::string &vertPath)
{
    mShaders[0] = createShader( loadShader(fragPath), GL_FRAGMENT_SHADER, fragPath );
    mShaders[1] = createShader( loadShader(vertPath), GL_VERTEX_SHADER, vertPath );

    mProgram = glCreateProgram();

    init();

}


Shader::~Shader()
{
    for (unsigned int i=0; i<(unsigned)ShaderType::COUNT; ++i)
    {
        glDetachShader(mProgram, mShaders[i]);
        glDeleteShader(mShaders[i]);
    }

    glDeleteProgram(mProgram);
}


void Shader::init()
{
    for (unsigned int i=0; i<(unsigned)ShaderType::COUNT; ++i)
        glAttachShader(mProgram, mShaders[i]);

    glBindAttribLocation(mProgram, 0, "position");

    glLinkProgram(mProgram);
    checkShaderError(
        mProgram,
        GL_LINK_STATUS,
        true,
        "Error: program linking failed"
    );
}


void Shader::bind()
{
    glUseProgram(mProgram);
}


void Shader::unbind()
{
    glUseProgram(0);
}


void Shader::setMat4(const std::string &name, const glm::mat4 &mat)
{
    GLint location = glGetUniformLocation(mProgram, name.c_str());
    glUniformMatrix4fv(location, 1, GL_FALSE, &mat[0][0]);
}


void Shader::setVec3(const std::string &name, const glm::vec3 &vec)
{
    GLint location = glGetUniformLocation(mProgram, name.c_str());
    glUniform3fv(location, 1, &vec[0]);
}


void Shader::checkShaderError(
    unsigned int shader,
    unsigned int flag,
    bool program,
    const std::string errorMSG,
    const std::string &fileName)
{
    int success = 0;
    char error[1024] = {0};

    if (program)
        glGetProgramiv(shader, flag, &success);
    else
        glGetShaderiv(shader, flag, &success);

    if (success)
        return;

    if (program)
        glGetProgramInfoLog(shader, sizeof(error), NULL, error);
    else
        glGetShaderInfoLog(shader, sizeof(error), NULL, error);

    std::cerr << "in file "
              << fileName
              << ":\n"
              << errorMSG
              << ": '"
              << error
              << "'\n\n";
}


std::string Shader::loadShader(const std::string &fileName)
{
    std::ifstream file(fileName);
    std::string output = "";

    if (file.fail())
    {
        std::cerr << "Failed to load "
                  << fileName
                  << std::endl;

        return output;
    }

    std::string line = "";
    while ( getline(file, line) )
    {
        output.append(line + "\n");
    }

    return output;
}


unsigned int Shader::createShader(
    const std::string &text,
    unsigned int shaderType,
    const std::string &fileName)
{
    unsigned int shaderID = glCreateShader(shaderType);

    if (shaderID == 0)
        std::cerr << "Shader creation failed.\n";

    const char* shaderString = text.c_str();
    glShaderSource(shaderID, 1, &shaderString, NULL);
    glCompileShader(shaderID);

    checkShaderError(
        shaderID,
        GL_COMPILE_STATUS,
        false,
        "Shader compilation failed",
        fileName
    );

    return shaderID;
}


