#ifndef TEXTURE_HH
#define TEXTURE_HH

#include <iostream>
#include <GL/glew.h>
#include <cassert>


class Texture
{
public:
    unsigned int id = 0;
    unsigned int width = 0;
    unsigned int height = 0;

    unsigned int internalFormat = 0;
    unsigned int imgFormat = 0;
    unsigned int wrapS = 0;
    unsigned int wrapT = 0;
    unsigned int filterMin = 0;
    unsigned int filterMax = 0;


    void free()
    {
        if (id == 0)
        {
            std::cout << "no texture\n";
            return;
        }

        glDeleteTextures(1, &id);
        id = 0;
    }


    void bind() const
    {
        assert (id != 0);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, id);
    }


    void unbind()
    {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

};


#endif
