#ifndef SHADER_HH
#define SHADER_HH

#include <GL/glew.h>
#include <string>
#include <vector>
#include <3rdParty/glm/glm/mat4x4.hpp>
#include <array>


enum class ShaderType: unsigned int
{
    FRAG,
    VERT,
    COUNT
};


class Shader
{
public:
    Shader(const std::string &fragPath, const std::string &vertPath);

    ~Shader();

    void init();
    void bind();
    void unbind();

    void setMat4(const std::string &name, const glm::mat4 &mat);
    void setVec3(const std::string &name, const glm::vec3 &vec);

private:
    void checkShaderError(unsigned int shader,
                          unsigned int flag,
                          bool program,
                          const std::string errorMSG,
                          const std::string &fileName="");

    std::string loadShader(const std::string &fileName);

    unsigned int createShader(const std::string &text,
                              unsigned int shaderType,
                              const std::string &fileName="");

    std::array<unsigned int, (unsigned long)ShaderType::COUNT> mShaders;
    unsigned int mProgram;
};

#endif // SHADER_HH
