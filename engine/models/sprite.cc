#include "sprite.hh"
#include <GL/glew.h>
#include <cassert>


Sprite::Sprite():
    state(ObjectState::Default),
    tex(nullptr),
    mesh(nullptr),
    shader(nullptr)
{}


Sprite::~Sprite()
{}

void Sprite::drawAndBind(Camera* cam)
{
    assert(tex != nullptr);
    assert(shader != nullptr);
    assert(mesh != nullptr);

    if (state != ObjectState::Alive)
        return;

    auto trans = glm::mat4(1.0f);
    trans = glm::translate(trans, model.pos);
    trans = glm::rotate(trans, glm::radians(model.rot.x), glm::vec3(1, 0, 0));
    trans = glm::rotate(trans, glm::radians(model.rot.y), glm::vec3(0, 1, 0));
    trans = glm::rotate(trans, glm::radians(model.rot.z), glm::vec3(0, 0, 1));
    trans = glm::scale(trans, model.scale);

    shader->bind();

    // TODO: avoid unnecessary proj and view calss by batching together:
    // - objs using same shader and view

    shader->setMat4("projection", cam->getProjection()); // only needed if fov/aspect/clip planes change
    shader->setMat4("view", cam->getView()); // only needed if camera moves
    shader->setMat4("model", trans);

    tex->bind();
    mesh->draw();

    tex->unbind();
    shader->unbind();
}


