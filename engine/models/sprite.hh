#ifndef SPRITE_HH
#define SPRITE_HH

#include "model.hh"
#include "../render/mesh.hh"
#include "../render/shader.hh"
#include "../render/texture.hh"
#include "../utility/camera.hh"
#include "../utility/enums.hh"

class Sprite
{
public:
    Sprite();
    ~Sprite();

    void drawAndBind(Camera* cam);

    ObjectState state;
    Model model;
    Texture* tex;
    Mesh* mesh;
    Shader* shader;
};


#endif
