#ifndef RECT_HH
#define RECT_HH

#include "model.hh"
#include "../render/mesh.hh"
#include "../render/shader.hh"
#include "../utility/camera.hh"
#include "../utility/enums.hh"


class Rect
{
public:
    Rect():
        state(ObjectState::Default),
        mesh(nullptr),
        shader(nullptr)
    {}

    ~Rect() {}

    void draw(Camera* cam)
    {
        assert (shader != nullptr);
        assert (mesh != nullptr);

        glm::mat4 trans = glm::mat4(1.0f);
        trans = glm::translate(trans, model.pos);
        trans = glm::rotate(trans, glm::radians(model.rot.x), glm::vec3(1, 0, 0));
        trans = glm::rotate(trans, glm::radians(model.rot.y), glm::vec3(0, 1, 0));
        trans = glm::rotate(trans, glm::radians(model.rot.z), glm::vec3(0, 0, 1));
        trans = glm::scale(trans, model.scale);

        shader->bind();
        shader->setMat4("projection", cam->getProjection());
        shader->setMat4("view", cam->getView());
        shader->setMat4("model", trans);
        shader->setVec3("color", model.color);

        mesh->draw();
        shader->unbind();
    }

    ObjectState state;
    Model model;
    Mesh* mesh;
    Shader* shader;
};


#endif
