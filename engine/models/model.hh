#ifndef MODEL_HH
#define MODEL_HH

#include <3rdParty/glm/glm/vec3.hpp>


struct Model
{
    glm::vec3 pos = glm::vec3(0.0f);
    glm::vec3 rot = glm::vec3(0.0f);
    glm::vec3 color = glm::vec3(0.0f);
    glm::vec3 scale = glm::vec3(1.0f);
};


#endif
